<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

class products extends Model
{
    use HasFactory;
    /**
     * fillable
     * @var array
     */

     protected $fillable=[

    'image', 'barcode', 'title', 'description',  'buy_price', 'sell_price', 'stock'
     ]; 
     
     /** 
      * Categories 
      *@return void
      */

      public function categories()
      {
        return $this->belongsTo(categories::class);
      }

      /** 
      * Details
      *@return void
      */

      public function Details()
      {
        return $this->hasMany(transaction_details::class);
      }

      /**
     * image
     * @return attribute
     */
    public function image(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => asset ('/storage/products/' . $value),
        );
        
    }
      
}