<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
Use Illuminate\Database\Eloquent\Casts\Attribute;

class profits extends Model
{
    use HasFactory;
     /**
     * fillable
     * 
     * @var array
     */

    protected $fillable = [

    'transaction_id', 'total'

    ];
    
    /** 
      * transaction 
      *@return void
      */

      public function Transaction()
      {
        return $this->belongsTo(transaction::class);
      }
    
      /**
     * createdAt
     * @return attribute
     */
    protected function createdAt(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Carbon:: parse($value)->format('d-M-Y H:i:s'),
        );
        
    }
}