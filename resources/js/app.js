import { createApp, h } from 'vue'
import { createInertiaApp } from '@inertiajs/inertia-vue3'
import { InertiaProgress } from '@inertiajs/progress';

createInertiaApp({
  resolve: name => require(`./Pages/${name}`),
  setup({ el, App, props, plugin }) {
    createApp({ render: () => h(App, props) })
      //set mixins
      .mixin({
        methods: {

          //method "hasAnyPermissions"
          hasAnyPermissions: function (permissions) {

            //get permissions from props
            let allPermissions = this.$page.props.auth.permissions;

            let hasPermissions = false;
            permissions,forEach(function(item){
              if(alPermissions[item]) hasPermissions = true;
            });
            
            return hasPermissions;
          }

        },
      })
      .use(plugin)
      .mount(el)
  },
});

InertiaProgress.init()

