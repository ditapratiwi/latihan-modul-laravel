<?php

use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\Apps;
use App\Http\Controllers\Apps\DashboardController;

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/login', function () {
//     return Inertia::render('login');
// });

// route home
Route::get('/', function () {
    return Inertia::render('Auth/Login');
})->middleware('guest');

// prefix "apps"
Route::prefix('apps')->group(function () {

    // middleware "auth"
    Route::group(['middleware' => ['auth']], function () {

        // route dashboard
        Route::get('dashboard', DashboardController::class)
            ->name('apps.dashboard');
    });
});


